<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT Mehod 1 : element by element
* @author AnneGF@CNRS
* @date : 2022-2023
* desc: Exemple applicable tel quel sur des fichiers TEI, prévu pour générer un fichier HTML
    incluant un appel à Bootstap (bibliothèque CSS permettant des facilités d'affichage).
    
    Voir : https://getbootstrap.com/docs/5.0/getting-started/introduction/
**/
-->
<!DOCTYPE tei2html [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="1.0">

    <xsl:output method="xhtml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!-- <xsl:strip-space elements="*"/>-->
    <!--<xsl:preserve-space elements="tei:ab tei:bibl tei:note tei:title"/>-->




    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
            <head>
                <title>
                    <!-- Ici, on va chercher explicitement la valeur d'un élément spécifique -->
                    <xsl:value-of
                        select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                </title>




                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>
                <script src="https://code.jquery.com/jquery-3.6.3.js"/>




                <style>
                    h1 {
                        text-align: center;
                        padding: 1em;
                        font-family: Garamond;
                        font-size: 300%;
                    }
                    
                    h2 {
                        text-align: center;
                        padding: 1em;
                        font-family: Garamond;
                        font-size: 170%;
                    }
                    
                    .text-end {
                        font-family: Garamond;
                        position: sticky;
                        top: 25;
                    }
                    
                    
                    p {
                        font-family: Garamond;
                        font-size: 130%;
                    }
                    
                    li {
                        font-family: Garamond;
                        font-size: 130%;
                        list-style-type: none;
                    }
                    

                    .dida {
                        font-family: Garamond;
                        font-size: 130%;
                        font-style: italic;
                        text-align: center;
                    }
                    
                    .lettrine {
                        border: 1px #496183;
                        font-family: Garamond;
                        font-size: 50px;
                        color: #496183;
                        padding: 0px 0px 0px 0;
                        margin-right: 5px;
                        float: left;
                    }
                    
                    .pageNum {
                        text-align: right;
                    }
                    
                    .persName {
                        color: #c25138;
                    }
                    
                    .personnage {
                        display: block;
                        font-family: Garamond;
                        text-align: center;
                    }
                    
                    .note {
                        font-family: Garamond;
                        font-style: italic;
                        font-size: 13pt;
                        display: inline-block;
                        float: right;
                        margin-left: 40%;
                        color: #5c5cad;
                    }
                    
                    .didaType {
                        font-style: italic;
                        color: #90168e;
                    }
                    
                    .fin {
                        font-size: 200%;
                        text-align: center;
                        margin: 3em;
                    }
                    
                    
                    .role {
                        margin: 2.5%;
                        font-size: 140%;
                        font-family: Garamond bold;
                        color: #5c5cad;
                    }
                    
                    /*
                    .terme {
                    display: none;
                    }
                    
                    .glossaire {
                    display: none;
                    }
                    */
                    
                    .reg,
                    .orig {
                        display: none;
                    }
                    
                    .app {
                        display: none;
                    }
                    
                    .view {
                        display: unset;
                        color: #5c5cad;
                    }
                    
                    #col {
                    column-count: 3;
                    }                     
                                                          
                
                </style>



            </head>
            <body style="margin-bottom: 15px; ">
                <div class="container">
                    <h1>
                        <xsl:value-of
                            select="tei:teiCorpus/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"
                        />
                    </h1>
                    <div style="text-align:center; font-family:Garamond; font-size: 150%;">par
                            <xsl:value-of
                            select="tei:teiCorpus/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:author"
                        /> (<xsl:value-of
                            select="tei:teiCorpus/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:date"
                        />)</div>
                    <br/>

                    <!-- Création de boutons pour switcher entre deux versions du texte -->
                    <div id="view-choice" class="text-end">
                        <input type="radio" class="btn-check" name="options-view" id="orig-view"
                            autocomplete="off" checked=""/>
                        <label class="btn btn-outline-secondary" for="orig-view">Original</label>
                        <input type="radio" class="btn-check" name="options-view" id="reg-view"
                            autocomplete="off"/>
                        <label class="btn btn-outline-secondary" for="reg-view">Régularisé</label>
                        <br/>
                        <br/>
                    </div>
                    <!-- Création d'un bouton pour switcher entre affichage de l'apparat critique et masquage de l'apparat critique -->
                    <div>
                        <input/>


                    </div>


                    <xsl:apply-templates/>

                    <!-- Fonction pour afficher un index des noms de personnes citées (la transformation fonctionne, mais génère un message d'erreur dans le fichier XSL) -->
                     <!--<div>
                        <h2>Index des noms de personnes citées</h2>
                        <xsl:for-each select="distinct-values(//tei:persName)">
                            <xsl:sort select="."/>
                            <p>
                                <xsl:value-of select="."/><xsl:value-of select="//tei:div[descendant::tei:persName]/tei:head"/>
                                
                            </p>    
                        </xsl:for-each>
                    </div>-->
                    
                    <!-- pour associer les noms de personnes dans l'index à l'acte où ils sont cités -->
                    <!-- //div[descendant::persName]/head -->
                    


                    <div>
                        <div type="glossaire">
                            <h2>Glossaire</h2>
                            <ul id="col">
                                <xsl:for-each
                                    select="//tei:note[child::tei:term][not(. = preceding::tei:note)]">
                                    <!--pour indiquer que l'élément enfant de <note> doit être <term> uniquement et de ne pas mettre de doublon dansla liste-->

                                    <xsl:sort select="."/>
                                    <li>
                                        <xsl:value-of select="./tei:term"/>
                                        <xsl:text> : </xsl:text>
                                        <xsl:value-of select="./tei:gloss"/>
                                    </li>

                                </xsl:for-each>

                            </ul>
                        </div>
                    </div>


                    <!-- Partie pour les notes de bas de page (uniquement celles qui n'ont pas d'enfant, c'est-à-dire qui ne correspondent pas à une indication scénique ou ou terme du glossaire) -->
                    <!--<h2>Notes de bas de page</h2>
                    <xsl:for-each select="distinct-values(//tei:note[not(child::*)])">
                   
                        <p>
                            <xsl:value-of select="."/>
                            
                        </p>    
                    </xsl:for-each>-->





                </div>

                <script>
                    
                    <!--Code proposé par Anne GF pour la mise en oeuvre JavaScript des boutons cliquables pour switcher entre les deux versions du texte -->
                    switchView();
                    document.getElementById("reg-view").onclick =  function() { switchView();};
                    document.getElementById("orig-view").onclick =  function() { switchView();};
                    function switchView() {
                    $( "#view-choice:has(#reg-view:checked) ~ * .reg" ).addClass( "view" );
                    $( "#view-choice:has(#reg-view:checked) ~ * .orig" ).removeClass( "view" );
                    $( "#view-choice:has(#orig-view:checked) ~ * .orig").addClass( "view" );
                    $( "#view-choice:has(#orig-view:checked) ~ * .reg").removeClass( "view" );
                    }                
                    
                    
                    
                </script>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="tei:teiCorpus">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:div">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:sp">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:l">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:c[@rend = 'lettrine']">
        <!--Isabelle ici on crée une classe lettrine dans le html de sortie à partir de élément c attribut rend=lettrine du TEI  -->
        <span class="lettrine">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!--ajout Isabelle personnage et numero acte en h2-->
    <!--les éléments qui contiennent une valeur ont la classe note ne fonctionne pas -->
    <!-- <xsl:template match="tei:note[not(child::*)]">
        <span class="note"><xsl:apply-templates/></span>
    </xsl:template>-->
    <!-- Pour app ou note qui ont des éléments enfants, castList, castGroup,castItem, role continue ton parcours -->
    <xsl:template
        match="tei:app | tei:note | tei:castList | tei:castGroup | tei:castItem | tei:role">
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="tei:head">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="tei:name">
        <span class="personnage">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <!--Isabelle didascalie valeur élement stage à revoir ne marche pas-->
    <!--<xsl:template match="tei:stage">
        <span class="dida">
            <xsl:apply-templates/>
        </span>
    </xsl:template>-->
    <!--si on fait un choix à l'intérieur des éléments stage pour prendre en compte les attributs who marche mais oblige à le faire pour chaque valeur, finalement abandonné car l'élément stage ne doit pas être autofermant et c'est la valeur qui sera affichée -->
    <xsl:template match="tei:stage">
        <xsl:choose>
            <xsl:when test="./@who = '#MED_nourrice'">
                <i>
                    <xsl:text>La nourrice</xsl:text>
                </i>
                <p>
                    <xsl:apply-templates/>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <span class="dida">
                    <xsl:apply-templates/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!--Isabelle chaque intervention d'un role -->
    <xsl:template match="tei:speaker">
        <p class="role">
            <xsl:apply-templates/>
        </p>
    </xsl:template>


    <xsl:template match="tei:persName">
        <span class="personneCitee">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:fw">
        <p class="pageNum">
            <text>--- </text>
            <xsl:apply-templates/>
            <text>---</text>
        </p>
    </xsl:template>
    <xsl:template match="tei:term">
        <span class="terme">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:gloss">
        <span class="glossaire">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:note[not(child::*)]">
        <span class="note">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:supplied">
        <span class="restitution">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:trailer">
        <p class="fin">
            <xsl:apply-templates/>
        </p>
    </xsl:template>



    <xsl:template match="tei:stage[@type!='setting']">
        <p class="didaType"><text>[</text><xsl:apply-templates/><text>]</text></p>
    </xsl:template>
    
    <xsl:template match="tei:stage[@type='setting']">
        <p class="didaType">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    
    <xsl:template match="tei:lg">
        
        <xsl:apply-templates/>
    </xsl:template>
    


    <!-- fin ajout -->
    <xsl:template match="tei:TEI">

        <xsl:apply-templates select="tei:text/*"/>
    </xsl:template>

    <!-- Pour front, body ou back, app ou note qui ont des éléments, castList, castGroup,castItem, role continue ton parcours -->
    <xsl:template match="tei:front | tei:body | tei:back">
        <xsl:apply-templates/>
    </xsl:template>


    <!-- Ne fait rien avec les tei:pb -->
    <xsl:template match="tei:pb"/>


    <!-- Template autofermant qui permet, pour un élément donné, de ne rien faire -->
    <xsl:template match="tei:teiHeader"/>


    <!-- Template qui permet pour chaque élément tei:p, de générer un élément HTML <p/>, 
        puis, concernant le contenu, d'appliquer les autres templates -->
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>



    <!-- Template qui s'applique à tei:hi, seulement si l'attribut @rend prend pour 
        valeur "italic" et qui génère un élément <i/> -->
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="tei:choice">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:orig">
        <span class="orig">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:reg">
        <span class="reg">
            <xsl:apply-templates/>
        </span>
    </xsl:template>




    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
