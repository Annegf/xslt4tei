<?xml version="1.0" encoding="UTF-8"?>
<!-- XSLT - Method 1 - Copy but
* @author AnneGF@CNRS
* @date : 2022-2023
* desc:     Cet exemple est applicable sur des fichiers TEI et prévu pour générer un fichier XML-TEI identique.
    À adapter bien sûr pour apporter les modifications ponctuelles nécessaires.
* warning: Il faut désactiver l'extension des attributs par défaut (sauf si vous souhaitez les ajouter bien sûr).
    Sous Oxygen : décocher l'option "Étendre les attributs par défaut ("-expand")"
    dans les options avancées du processeurs Saxon. Cliquer sur la petite molette juste à côté du choix du processeurs
    Plus d'info ici :
    - le "problème" que ça pose et sa solution: https://stackoverflow.com/questions/70924771/unwanted-new-attributes-in-xml-output-after-xslt
    - une explication concernant la DTD mais valable aussi pour des schémas RNG, etc. : https://stackoverflow.com/questions/12800901/why-is-xslt-adding-attributes-itself-in-copy
* note: Il sagit ici de XSLT 2.0 (utilisation par ex des fonctions replace(), exists()...)
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="xml" indent="yes"/>

    <!-- Match nodes, processing-instruction and comments -->
    <xsl:template match="* | comment() | processing-instruction()">
        <!-- Copy the element -->
        <xsl:copy>
            <!-- Copy its attributes -->
            <xsl:for-each select="attribute::*">
                <xsl:attribute name="{name(.)}" select="."/>
            </xsl:for-each>
            <!-- Go on in the XML tree -->
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>


    <!-- Examples: remove mode="DONT_DO" if you want to use them -->
    <!-- Remplacement des balises <persName/> par des balises <name type="person"/> -->
    <xsl:template match="tei:persName" mode="DONT_DO">
        <!-- Create <cit> with attribute @corresp unchanged -->
        <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="type">
                <xsl:text>person</xsl:text>
            </xsl:attribute>
            <xsl:if test="exists(@type)">
                <xsl:attribute name="subtype" select="@type"/>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:note" mode="DONT_DO">
        <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
            <!--<xsl:copy-of select="@*"/>-->
            <!-- décommenter si les notes ont des attributs à copier -->
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="text()" mode="DONT_DO">
        <xsl:value-of select="replace(., '#([^ ]+) +', '$1/')"/>
    </xsl:template>
</xsl:stylesheet>
