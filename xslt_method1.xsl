<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT Mehod 1 : element by element
* @author AnneGF@CNRS
* @date : 2022-2023
* desc: Exemple applicable tel quel sur des fichiers TEI, prévu pour générer un fichier HTML
    incluant un appel à Bootstap (bibliothèque CSS permettant des facilités d'affichage).
    
    Voir : https://getbootstrap.com/docs/5.0/getting-started/introduction/
**/
-->
<!DOCTYPE tei2html [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="1.0">

    <xsl:output method="xhtml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!-- <xsl:strip-space elements="*"/>-->
    <!--<xsl:preserve-space elements="tei:ab tei:bibl tei:note tei:title"/>-->

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
            <head>
                <title>
                    <!-- Ici, on va chercher explicitement la valeur d'un élément spécifique -->
                    <xsl:value-of
                        select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                </title>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>
                <style>
                    h1,
                    h2 {
                        text-align: center;
                        padding: 1em;
                    }</style>
            </head>
            <body style="margin-bottom: 15px;">
                <div class="container">
                    <h1>
                        <xsl:value-of
                            select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                    </h1>
                    <xsl:apply-templates/>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="tei:TEI">
        <xsl:apply-templates select="tei:text/tei:body/*"/>
    </xsl:template>

    <!-- Template autofermant qui permet, pour un élément donné, de ne rien faire -->
    <xsl:template match="tei:teiHeader"/>

    <!-- Template qui permet pour chaque élément tei:p, de générer un élément HTML <p/>, 
        puis, concernant le contenu, d'appliquer les autres templates -->
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <!-- Template qui s'applique à tei:hi, seulement si l'attribut @rend prend pour 
        valeur "italic" et qui génère un élément <i/> -->
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>


    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
