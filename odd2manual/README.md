Créé un manuel au format HTML à partir d'un fichier ODD

Exemples d'usage en ligne de commande :
```bash
saxonb-xslt $ODD_FILE odd2manual.xsl >$HTML_FILE
saxonb-xslt $ODD_FILE odd2manual.xsl [SHOW-WARNINGS=0] [STAND-ALONE=0]0123 >$HTML_FILE
```

Paramètres disponibles :
  * STAND-ALONE (par défaut 1) : génère une page HTML complète avec appel à Bootstrap
  * SHOW-WARNINGS (par défaut 1) : affiche des message d'alerte si l'ODD ne semble pas assez documenté

Exemples de fichiers
  * ODD_Lobineau.odd : fichier ODD d'entrée
  * ODD_Lobineau.html : fichier HTML généré

ls ~/Data/cloudUGA/CDD_Fanny/Création_ODD/*/*.odd | while read odd_file; do html_file=`basename "$odd_file"`".html"; saxonb-xslt "$odd_file" ./odd2manual.xsl SHOW-WARNINGS=1 >$html_file; done