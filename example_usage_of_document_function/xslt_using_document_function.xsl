<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <!--
/**
* XSLT for showing how to use document(). Useful when you need to manipulate several input file
* @author AnneGF@CNRS
* @date : 2022-2023
**/
-->
    
    <!-- Example: absolute path in linux distribution:
    <xsl:variable name="nakala"
        select="document('/home/annegf/Nextcloud/ELAN/Projets/Reticence/data/reticence_data.xml')"/>
    -->
    
    <xsl:variable name="fichier1"
        select="document('fichier_test1.xml')"/>
    <xsl:variable name="fichier2"
        select="document('fichier_test2.xml')"/>
    <xsl:template match="/">
        <xsl:message>
            <xsl:text>info 1 : </xsl:text>
            <xsl:value-of select="$fichier1/root/info"/>
        </xsl:message>
        <xsl:message>
            <xsl:text>info 2 : </xsl:text>
            <xsl:value-of select="$fichier2/root/info"/>
        </xsl:message>
        
    </xsl:template>
</xsl:stylesheet>
